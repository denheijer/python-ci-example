def add_two_numbers(i, j):
    return i + j

def multiply_two_numbers(i, j):
    return i*j


def test_add_two_numbers():
    assert add_two_numbers(2, 3) == 5

def test_multiply_two_numbers():
    assert multiply_two_numbers(2, 3) == 6
